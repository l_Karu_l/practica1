﻿using System;

namespace Practica_1
{
    class Program
    {
        static void Main(string[] args)
        {
            ejercicio5();
        }


        public static void ejercicio1()
        {
            int numaprobados = 0;
            int numsobresalientes = 0;
            double nota;

            for (int i = 0; i < 10; i++)
            {
                nota = Double.Parse(Console.ReadLine());
                if (nota >= 9)
                {
                    numsobresalientes++;
                }
                else if (nota >= 5)
                {
                    numaprobados++;
                }

            }


            Console.WriteLine("El numero de aprobados es:" + numaprobados);
            Console.WriteLine("El numero de sobrsalientes es:" + numsobresalientes);
        }
















        public static void ejercicio2()
        {
            int dia, mes, año, mesnuevo, aux;
            dia = Int32.Parse(Console.ReadLine());
            mes = Int32.Parse(Console.ReadLine());
            año = Int32.Parse(Console.ReadLine());


            if (dia < 1 || dia > 30 || mes > 12 || mes < 1)
            {
                Console.WriteLine("No has insertado una fecha correctamente");
            }
            else
            {
                mesnuevo = mes + 3;
                if (mesnuevo > 12)
                {
                    aux = mesnuevo - 12;
                    mesnuevo = aux;
                }
                Console.WriteLine("La fecha de pago es: " + dia + "/" + mesnuevo + "/" + año);

            }

        }














        public static void ejercicio3()
        {
            int i;
            int[] numeros = new int[10];

            numeros[0] = 5;
            numeros[1] = 22;
            numeros[2] = 30;
            numeros[3] = 48;
            numeros[4] = 62;
            numeros[5] = 71;
            numeros[6] = 85;
            numeros[7] = 99;
            numeros[8] = 123;
            numeros[9] = 147;

            for (i = 0; i < numeros.Length; i++)
            {
                Console.WriteLine("El numero es " + numeros[i]);
            }

            while (i > 0)
            {
                i--;
                Console.WriteLine("El numero es " + numeros[i]);
            }
            Console.ReadKey();
        }













        public static void ejercicio5()
        {
            double n1, n2, n3, n4, n5, n6, n7, n8, n9, n10;
            double mayor;
            Console.WriteLine("Inserte 10 numeros");



            n1 = Int32.Parse(Console.ReadLine());
            n2 = Int32.Parse(Console.ReadLine());
            n3 = Int32.Parse(Console.ReadLine());
            n4 = Int32.Parse(Console.ReadLine());
            n5 = Int32.Parse(Console.ReadLine());
            n6 = Int32.Parse(Console.ReadLine());
            n7 = Int32.Parse(Console.ReadLine());
            n8 = Int32.Parse(Console.ReadLine());
            n9 = Int32.Parse(Console.ReadLine());
            n10 = Int32.Parse(Console.ReadLine());



            Console.WriteLine("Numero mayor: " + Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n8, Math.Max(n9, n10))))))))));
            mayor = Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n8, Math.Max(n9, n10)))))))));



            if (mayor == n1)
            {
                Console.WriteLine("y 2º numero mayor : " + Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n8, Math.Max(n9, n10)))))))));
            }
            if (mayor == n2)
            {
                Console.WriteLine("y 2º numero mayor : " + Math.Max(n1, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n8, Math.Max(n9, n10)))))))));
            }
            if (mayor == n3)
            {
                Console.WriteLine("y 2º numero mayor : " + Math.Max(n1, Math.Max(n2, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n8, Math.Max(n9, n10)))))))));
            }
            if (mayor == n4)
            {
                Console.WriteLine("y 2º numero mayor : " + Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n8, Math.Max(n9, n10)))))))));
            }
            if (mayor == n5)
            {
                Console.WriteLine("y 2º numero mayor : " + Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n6, Math.Max(n7, Math.Max(n8, Math.Max(n9, n10)))))))));
            }
            if (mayor == n6)
            {
                Console.WriteLine("y 2º numero mayor : " + Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n7, Math.Max(n8, Math.Max(n9, n10)))))))));
            }
            if (mayor == n7)
            {
                Console.WriteLine("y 2º numero mayor : " + Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n8, Math.Max(n9, n10)))))))));
            }
            if (mayor == n8)
            {
                Console.WriteLine("y 2º numero mayor : " + Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n9, n10)))))))));
            }
            if (mayor == n9)
            {
                Console.WriteLine("y 2º numero mayor : " + Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n8, n10)))))))));
            }
            if (mayor == 10)
            {
                Console.WriteLine("y 2º numero mayor : " + Math.Max(n1, Math.Max(n2, Math.Max(n3, Math.Max(n4, Math.Max(n5, Math.Max(n6, Math.Max(n7, Math.Max(n8, n9)))))))));
            }
        }














        public static void ejercicio6()
        {
            double n1, n2, n3, n4, n5, n6, n7, n8, n9, n10;
            double diferencia = 10;


            Console.WriteLine("Inserta 10 números");



            n1 = Int32.Parse(Console.ReadLine());
            n2 = Int32.Parse(Console.ReadLine());
            n3 = Int32.Parse(Console.ReadLine());
            n4 = Int32.Parse(Console.ReadLine());
            n5 = Int32.Parse(Console.ReadLine());
            n6 = Int32.Parse(Console.ReadLine());
            n7 = Int32.Parse(Console.ReadLine());
            n8 = Int32.Parse(Console.ReadLine());
            n9 = Int32.Parse(Console.ReadLine());
            n10 = Int32.Parse(Console.ReadLine());




            if (n1 == n2 | n1 == n3 | n1 == n4 | n1 == n5 | n1 == n6 | n1 == n7 | n1 == n8 | n1 == n9 | n1 == n10)
            {
                diferencia--;
            }
            if (n2 == n1 | n2 == n3 | n2 == n4 | n2 == n5 | n2 == n6 | n2 == n7 | n2 == n8 | n2 == n9 | n2 == n10)
            {
                diferencia--;
            }
            if (n3 == n2 | n1 == n3 | n3 == n4 | n3 == n5 | n3 == n6 | n3 == n7 | n3 == n8 | n3 == n9 | n3 == n10)
            {
                diferencia--;
            }
            if (n4 == n2 | n4 == n3 | n1 == n4 | n4 == n5 | n4 == n6 | n4 == n7 | n4 == n8 | n4 == n9 | n4 == n10)
            {
                diferencia--;
            }
            if (n5 == n2 | n5 == n3 | n5 == n4 | n1 == n5 | n5 == n6 | n5 == n7 | n5 == n8 | n5 == n9 | n5 == n10)
            {
                diferencia--;
            }
            if (n6 == n2 | n6 == n3 | n6 == n4 | n6 == n5 | n1 == n6 | n6 == n7 | n6 == n8 | n6 == n9 | n6 == n10)
            {
                diferencia--;
            }
            if (n7 == n2 | n7 == n3 | n7 == n4 | n7 == n5 | n7 == n6 | n1 == n7 | n7 == n8 | n7 == n9 | n7 == n10)
            {
                diferencia--;
            }
            if (n8 == n2 | n8 == n3 | n8 == n4 | n8 == n5 | n8 == n6 | n8 == n7 | n1 == n8 | n8 == n9 | n8 == n10)
            {
                diferencia--;
            }
            if (n9 == n2 | n9 == n3 | n9 == n4 | n9 == n5 | n9 == n6 | n9 == n7 | n9 == n8 | n1 == n9 | n9 == n10)
            {
                diferencia--;
            }
            if (n10 == n2 | n10 == n3 | n10 == n4 | n10 == n5 | n10 == n6 | n10 == n7 | n10 == n8 | n10 == n9)
            {
                diferencia--;
            }

            Console.WriteLine("Tenemos " + diferencia + " números distintos");
        }
    }
}
